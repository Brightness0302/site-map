/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    theme: {
        fontFamily: {
            sans: ["Source Sans Pro"],
        },
        screens: {
            "2xl": "1600px",
            xl: "1300px",
            lg: "1200px",
            md: "992px",
            sm: "640px",
            xm: "576px",
        },
        extend: {
            colors: {
                sideColor: "#F7F7F9",
                menuIconColor: "rgba(27, 31, 59, 0.5)",
                menutextColor: "rgba(27, 31, 59, 0.8)",
                activeColor: "#26ABE0",
                grayColor: "#1B1F3B59",
                activeDColor: "#32D7AD",
                warningDColor: "#F6576F",
                layoutactiveColor: "#26ABE011",
                layouthoverColor: "#26ABE020",
            },
            backgroundImage: {
                logo: "url('/img/logo.jpg')",
            },
            fontSize: {
                nsm: "13px",
            },
        },
    },
    plugins: [],
};
