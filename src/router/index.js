
import { createRouter, createWebHashHistory } from 'vue-router'

import dashboard from './modules/dashboard'
import Chart from './modules/chart'
import Device from './modules/device'
import Group from './modules/group'
import History from './modules/history'
import Localive from './modules/localive'
import Notification from './modules/notification'
import Report from './modules/report'
import Rule from './modules/rule'
import Site from './modules/site'
import Vector from './modules/vector'
import login from './modules/login'
import checkLicense from './modules/checkLicense'
import inActive from './modules/inActive'
import landingView from './modules/landingView'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
    },
    ...landingView, 
    ...login, 
    ...checkLicense, 
    ...inActive, 

    ...dashboard,
    ...Chart,
    ...Device,
    ...Group,
    ...History,
    ...Localive,
    ...Notification,
    ...Report,
    ...Rule,
    ...Site,
    ...Vector,
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { left: 0, top: 0 }
    }
  },
})
router.beforeEach(async (to) => {
 
});
export default router
