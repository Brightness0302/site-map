const Layout = () => import("@/layout/index.vue");
const Group = () => import("@/views/group/index.vue");
const Users = () => import("@/views/group/users.vue");
const Roles = () => import("@/views/group/roles.vue");

const AddUser = () => import("@/views/group/adduser.vue");
const EditUser = () => import("@/views/group/edituser.vue");

const AddRole = () => import("@/views/group/addrole.vue");
const EditRole = () => import("@/views/group/editrole.vue");

export default [
    {
        path: "/",
        component: Layout,
        children: [
            {
                path: "group",
                name: "group",
                component: Group,
                meta: {
                    title: "group",
                    affix: true,
                },
                children: [
                    {
                        path: "", // Add this empty path
                        name: "Roles",
                        component: Roles,
                        meta: {
                            title: "Users Page",
                            affix: true,
                        },
                    },
                    {
                        path: "users",
                        name: "Users",
                        component: Users,
                        meta: {
                            title: "Users Page",
                            affix: true,
                        },
                    },
                    {
                        path: "roles",
                        component: Roles,
                        meta: {
                            title: "Roles Page",
                            affix: true,
                        },
                    },
                    {
                        path: "adduser",
                        name: "AddUser",
                        component: AddUser,
                        meta: {
                            title: "AddUser Page",
                            affix: true,
                        },
                    },
                    {
                        path: "edituser/:user_id",
                        name: "EditUser",
                        component: EditUser,
                        props: true,
                        meta: {
                            title: "EditUser Page",
                            affix: true,
                        },
                    },
                    {
                        path: "addrole",
                        name: "AddRole",
                        component: AddRole,
                        props: true,
                        meta: {
                            title: "AddRole Page",
                            affix: true,
                        },
                    },
                    {
                        path: "editrole/:role_id",
                        name: "EditRole",
                        component: EditRole,
                        props: true,
                        meta: {
                            title: "EditRole Page",
                            affix: true,
                        },
                    },
                ],
            },
        ],
    },
];
