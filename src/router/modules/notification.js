const Notification = () => import('@/views/notification/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'notification',
        name: 'notification',
        component: Notification,
        meta: {
          title: 'notification',
          affix: true,
        },
      },
    ],
  },
]
