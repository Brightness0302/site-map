const Login = () => import('@/views/login/index.vue')

export default [
  {
    path: '/',
    children: [
      {
        path: 'login',
        name: 'login',
        component: Login,
        meta: {
          title: 'login',
          affix: true,
        },
      },
    ],
  },
]
