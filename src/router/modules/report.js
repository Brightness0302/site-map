const Report = () => import('@/views/report/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/report',
    component: Layout,
    children: [
      {
        path: '',
        name: 'report',
        component: Report,
        meta: {
          title: 'report',
          affix: true,
        },
      },
    ],
  },
]
