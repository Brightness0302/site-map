const landingView = () => import('@/views/landingView/index.vue')

export default [
  {
    path: '/',
    children: [
      {
        path: 'landingView',
        name: 'landingView',
        component: landingView,
        meta: {
          title: 'landingView',
          affix: true,
        },
      },
    ],
  },
]
