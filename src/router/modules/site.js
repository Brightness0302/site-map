const Site = () => import('@/views/site/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'site',
        name: 'site',
        component: Site,
        meta: {
          title: 'site',
          affix: true,
        },
      },
    ],
  },
]
