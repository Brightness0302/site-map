const Rule = () => import('@/views/rule/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'rule',
        name: 'rule',
        component: Rule,
        meta: {
          title: 'rule',
          affix: true,
        },
      },
    ],
  },
]
