const Device = () => import("@/views/device/index.vue");
const Layout = () => import("@/layout/index.vue");
const Dashboard = () => import("@/views/device/dashboard.vue");

export default [
    {
        path: "/",
        component: Layout,
        children: [
            {
                path: "device",
                name: "device",
                component: Device,
                meta: {
                    title: "device",
                    affix: true,
                },
                children: [
                    {
                        path: "", // Add this empty path
                        name: "DeviceDashboard",
                        component: Dashboard,
                        meta: {
                            title: "Device-Dashboard Page",
                            affix: true,
                        },
                    },
                    {
                        path: "index",
                        component: Dashboard,
                        meta: {
                            title: "Device-Dashboard Page",
                            affix: true,
                        },
                    },
                ],
            },
        ],
    },
];
