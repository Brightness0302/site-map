const Chart = () => import('@/views/chart/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'chart',
        name: 'chart',
        component: Chart,
        meta: {
          title: 'chart',
          affix: true,
        },
      },
    ],
  },
]
