const CheckLicense = () => import('@/views/login/checkLicense.vue')

export default [
  {
    path: '/',
    children: [
      {
        path: 'checklicense',
        name: 'checklicense',
        component: CheckLicense,
        meta: {
          title: 'checklicense',
          affix: true,
        },
      },
    ],
  },
]
