const inActive = () => import('@/views/login/inActive.vue')

export default [
  {
    path: '/',
    children: [
      {
        path: 'inActive',
        name: 'inactive',
        component: inActive,
        meta: {
          title: 'InActive',
          affix: true,
        },
      },
    ],
  },
]
