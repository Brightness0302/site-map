const Vector = () => import('@/views/vector/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'vector',
        name: 'vector',
        component: Vector,
        meta: {
          title: 'vector',
          affix: true,
        },
      },
    ],
  },
]
