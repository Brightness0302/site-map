const Localive = () => import('@/views/localive/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'localive',
        name: 'localive',
        component: Localive,
        meta: {
          title: 'localive',
          affix: true,
        },
      },
    ],
  },
]
