const History = () => import('@/views/history/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'history',
        name: 'history',
        component: History,
        meta: {
          title: 'history',
          affix: true,
        },
      },
    ],
  },
]
