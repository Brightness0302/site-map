const Dashboard = () => import('@/views/dashboard/index.vue')
const Layout = () => import('@/layout/index.vue')

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
          title: 'dashboard',
          affix: true,
        },
      },
    ],
  },
]
