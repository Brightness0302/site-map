import Vuex from "vuex";

import api from "../utils/api_user";

const authStore = {
    namespaced: true,
    state: {
        // your state properties go here
        user: null,
        isAuthenticated: false,
    },
    mutations: {
        // your mutations go here
        login(state, user) {
            state.isAuthenticated = true;
            state.user = user;
        },
        logout(state) {
            state.isAuthenticated = false;
            state.user = null;
        },
    },
    actions: {
        // your actions go here
        login: async function ({ commit }) {
            try {
                const response = await api.get("/getinfo");
                const user = response.data.data;
                commit("login", user);
                return user;
            } catch (error) {
                console.log(error);
                throw error;
            }
        },
        logout: function ({ commit }) {
            commit("logout");
        },
    },
    getters: {
        // your getters go here
    },
};

export default authStore;
