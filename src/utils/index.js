export const removeById = (array, id) => {
    return array.filter((item) => item.id !== id);
};
