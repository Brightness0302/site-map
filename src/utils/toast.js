import Swal from "sweetalert2";
const toast = (Vue, type, msg) => {
    Vue.$swal
        .mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener("mouseenter", Swal.stopTimer);
                toast.addEventListener("mouseleave", Swal.resumeTimer);
            },
        })
        .fire({
            icon: type,
            title: msg,
        });
};
export default toast;
