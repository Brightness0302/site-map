import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src"),
        },
    },
    build: {
        chunkSizeWarningLimit: 2000,
        rollupOptions: {
            output: {
                manualChunks(id) {
                    if (id.includes("node_modules")) {
                        return id
                            .toString()
                            .split("node_modules/")[1]
                            .split("/")[0]
                            .toString();
                    }
                },
            },
        },
        splitChunks: {
            chunks: "all",
        },
    },
    server: {
        proxy: {
            "/api/v1/userservice": {
                target: "http://192.168.126.36:8080",
                changeOrigin: true,
                secure: false,
            },
            "/api/v1/sitemanagerservice": {
                target: "http://192.168.126.36:8082",
                changeOrigin: true,
                secure: false,
            },
        },
    },
});
